//+------------------------------------------------------------------+
//|                                                  my-first-ea.mq4 |
//|                                                        myegane49 |
//|                                           https://www.adorian.ir |
//+------------------------------------------------------------------+
#property copyright "myegane49"
#property link      "https://www.adorian.ir"
#property version   "1.00"
#property strict

#include <my-custom-functions.mqh>
#include <stderror.mqh>
#include <stdlib.mqh>

double risk_per_trade = 0.02;
//int count_candles = 240;
int number_magic = 1002;
int open_order_id;
double lot_size;
int period_resist = 75;
int period_signal = 4;
double signal_open_gap_pips = 25;
double signal_close_gap_pips = 5;
int bb_std = 10;
int bb_period = 20;
datetime prev_tick_close_time;

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
    if (TimeCurrent() >= prev_tick_close_time) {
      double ema_support = iMA(NULL, 0, period_resist, 0, MODE_EMA, PRICE_CLOSE, 1);
      double ema_signal = iMA(NULL, 0, period_signal, 0, MODE_EMA, PRICE_CLOSE, 1);
      double gap_pips = MathRound(MathAbs(ema_support - ema_signal) / HowMuchAPipIs());
      
      if (!IsOpenPositionByMagicNumber(number_magic)) {
        if (gap_pips >= signal_open_gap_pips && ema_support > ema_signal) { // long position
          double bb_lower = iBands(NULL, 0, bb_period, bb_std, 0, PRICE_CLOSE, MODE_LOWER, 0);       
          lot_size = OptimalLotSize(risk_per_trade, Ask, bb_lower);
          open_order_id = OrderSend(NULL, OP_BUY, lot_size, Ask, 10, bb_lower, 0, NULL, number_magic);
          if (open_order_id < 0) {
            string error = "error ----------> " + ErrorDescription(GetLastError());
            StringToUpper(error);
            Print(error);
          }
        } else if (gap_pips >= signal_open_gap_pips && ema_support < ema_signal) { // short position
          double bb_upper = iBands(NULL, 0, bb_period, bb_std, 0, PRICE_CLOSE, MODE_UPPER, 0);
          lot_size = OptimalLotSize(risk_per_trade, Bid, bb_upper);
          open_order_id = OrderSend(NULL, OP_SELL, lot_size, Bid, 10, bb_upper, 0, NULL, number_magic);
          if (open_order_id < 0) {
            string error = "error ----------> " + ErrorDescription(GetLastError());
            StringToUpper(error);
            Print(error);
          }
        }
      } else if (OrderSelect(open_order_id, SELECT_BY_TICKET) && gap_pips <= signal_close_gap_pips) { // close position
        if (TimeCurrent() > OrderOpenTime() + 24 * 60 * 60 && OrderType() == 0) {
          OrderClose(open_order_id, lot_size, Bid, 10);
        } else if (TimeCurrent() > OrderOpenTime() + 24 * 60 * 60 && OrderType() == 1) {
          OrderClose(open_order_id, lot_size, Ask, 10);
        }
        
        if (OrderType() == 0) {
          OrderClose(open_order_id, lot_size, Bid, 10);
        } else if (OrderType() == 1) {
          OrderClose(open_order_id, lot_size, Ask, 10);
        }
      }
    }
    prev_tick_close_time = Time[0] + 5 * 60;
    
  }
