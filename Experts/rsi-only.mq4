//+------------------------------------------------------------------+
//|                                            optimized-ema-gap.mq4 |
//|                                                        myegane49 |
//|                                           https://www.adorian.ir |
//+------------------------------------------------------------------+
#property copyright "myegane49"
#property link      "https://www.adorian.ir"
#property version   "1.00"
#property strict

#include <my-custom-functions.mqh>
#include <stderror.mqh>
#include <stdlib.mqh>

double risk_per_trade = 0.02;
int number_magic = 1000;
int open_order_id;
double lot_size;
int rsi_period = 7;
int rsi_up = 85;
int rsi_low = 15;
int bb_std = 8;
int bb_period = 20;
datetime prev_tick_close_time;

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
    if (TimeCurrent() >= prev_tick_close_time) {
      double rsi = iRSI(NULL, 0, rsi_period, PRICE_CLOSE, 1);
      double rsi_prev = iRSI(NULL, 0, rsi_period, PRICE_CLOSE, 2);
      
      if (!IsOpenPositionByMagicNumber(number_magic)) {
        if (rsi_prev < rsi_low && rsi > rsi_low) { // long position
          double bb_lower = iBands(NULL, 0, bb_period, bb_std, 0, PRICE_CLOSE, MODE_LOWER, 0);
          double bb_middle = iBands(NULL, 0, bb_period, bb_std, 0, PRICE_CLOSE, MODE_MAIN, 0);      
          lot_size = OptimalLotSize(risk_per_trade, Ask, bb_lower);
          open_order_id = OrderSend(NULL, OP_BUY, lot_size, Ask, 10, bb_lower, bb_middle, NULL, number_magic);
          if (open_order_id < 0) {
            string error = "error ----------> " + ErrorDescription(GetLastError());
            StringToUpper(error);
            Print(error);
          }
        } else if (rsi_prev > rsi_up && rsi < rsi_up) { // short position
          double bb_upper = iBands(NULL, 0, bb_period, bb_std, 0, PRICE_CLOSE, MODE_UPPER, 0);
          double bb_middle = iBands(NULL, 0, bb_period, bb_std, 0, PRICE_CLOSE, MODE_MAIN, 0); 
          lot_size = OptimalLotSize(risk_per_trade, Bid, bb_upper);
          open_order_id = OrderSend(NULL, OP_SELL, lot_size, Bid, 10, bb_upper, bb_middle, NULL, number_magic);
          if (open_order_id < 0) {
            string error = "error ----------> " + ErrorDescription(GetLastError());
            StringToUpper(error);
            Print(error);
          }
        }
      } else if (OrderSelect(open_order_id, SELECT_BY_TICKET)) { // close position
        if (TimeCurrent() > OrderOpenTime() + 24 * 60 * 60 && OrderType() == 0) {
          OrderClose(open_order_id, lot_size, Bid, 10);
        } else if (TimeCurrent() > OrderOpenTime() + 24 * 60 * 60 && OrderType() == 1) {
          OrderClose(open_order_id, lot_size, Ask, 10);
        }
      }
    
    }
    prev_tick_close_time = Time[0] + 5 * 60;
 
  }
