//+------------------------------------------------------------------+
//|                                          my-custom-functions.mqh |
//|                                                        myegane49 |
//|                                           https://www.adorian.ir |
//+------------------------------------------------------------------+
#property copyright "myegane49"
#property link      "https://www.adorian.ir"
#property strict
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex5"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+

double HowMuchAPipIs() {
  if (_Digits >= 4) {
    return 0.0001;
  } else {
    return 0.01;
  }
}

double OptimalLotSize(double maxLossPerc, int maxLossInPips) {
  double accEquity = AccountEquity();
  double lotSize = MarketInfo(NULL, MODE_LOTSIZE);
  double tickValue = MarketInfo(NULL, MODE_TICKVALUE);
  if (Digits <= 3) {
    tickValue = tickValue / 100;
  }
  
  double maxLossDollar = accEquity * maxLossPerc;
  double maxLossInQuoteCurr = maxLossDollar / tickValue;
  double optimalLotSize = NormalizeDouble(maxLossInQuoteCurr / (maxLossInPips * HowMuchAPipIs()) / lotSize, 2);
  
  return optimalLotSize;
}

double OptimalLotSize(double maxLossPerc, double entryPrice, double slPrice) {
  int maxLossInPips = MathFloor(MathAbs(entryPrice - slPrice) / HowMuchAPipIs());
  return OptimalLotSize(maxLossPerc, maxLossInPips);
}

bool IsOpenPositionByMagicNumber(int magicNumber) {
  int openOrders = OrdersTotal();
  for (int i = 0; i < openOrders; i++) {
    if (OrderSelect(i, SELECT_BY_POS)) {
      if (OrderMagicNumber() == magicNumber) return true;
    }
  }
  return false;
}

void DrawTrendLine() {
  //int candle1 = FirstCandle();
  //int candle2 = SecondCandle();
    
  //ObjectCreate(0, "lower_trend", OBJ_TREND, 0, Time[candle1], Low[candle1], Time[candle2], Low[candle2]);
}