//+------------------------------------------------------------------+
//|                                              risk-management.mq4 |
//|                                                        myegane49 |
//|                                           https://www.adorian.ir |
//+------------------------------------------------------------------+
#property copyright "myegane49"
#property link      "https://www.adorian.ir"
#property version   "1.00"
#property strict
#property show_inputs

#include <mohsen-include/custom-functions.mqh>
//+------------------------------------------------------------------+
//| Script program start function                                    |
//+------------------------------------------------------------------+
input double maxLossPerc = 0.02;
input double entryPrice;
input double slPrice;

void OnStart()
  {
    //Alert(MarketInfo(NULL, MODE_MINLOT));
    Alert(OptimalLotSize(maxLossPerc, entryPrice, slPrice));
    //Alert(OptimalLotSize(0.03, 17));
  }
  
  double OptimalLotSize(double maxLossPerc, int maxLossInPips) {
      double accEquity = AccountEquity();
      //double accEquity = 1000;
      double lotSize = MarketInfo(NULL, MODE_LOTSIZE);
      double tickValue = MarketInfo(NULL, MODE_TICKVALUE);
      if (_Digits <= 3) {
        tickValue = tickValue / 100;
      }
      double maxLossDollar = accEquity * maxLossPerc;
      double maxLossInQuoteCurr = maxLossDollar / tickValue;
      //double optimalLotSize = NormalizeDouble(maxLossInQuoteCurr / (maxLossInPips * HowMuchAPipIs()) / lotSize, 2);
      double optimalLotSize = maxLossInQuoteCurr / (maxLossInPips * HowMuchAPipIs()) / lotSize;
      
      return optimalLotSize;
  }
  
  double OptimalLotSize(double maxLossPerc, double entryPrice, double slPrice) {
    int maxLossInPips = MathAbs(entryPrice - slPrice) / HowMuchAPipIs();
    return OptimalLotSize(maxLossPerc, maxLossInPips);
  }
//+------------------------------------------------------------------+
